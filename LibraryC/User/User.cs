﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryC.User
{
    public class User
    {
        /// <summary>
        /// State Variables
        /// </summary>
        private string name;
        private string lastName;
        private string username;
        private string password;
        private string access;
        private static int count;
        private int id;
        private bool active;

        /// <summary>
        /// Constructer of the user
        /// </summary>
        /// <param name="name">Name of the user</param>
        /// <param name="lastName">Last name of the user</param>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <param name="tokenAcess">Acess token to see if it is a manager or a employ</param>
        /// <param name="id">Id of the user</param>
        /// <param name="active">if the user is active or nor</param>
        public User(string name, string lastName, string username, string password, string access, int id, bool active)
        {
            this.name = name;
            this.lastName = lastName;
            this.username = username;
            this.password = password;
            this.access = access;
            this.id = id;
            this.active = active;
            count++;
        }

        /// <summary>
        /// Proprieties
        /// </summary>
        public static int Count
        {
            get
            {
                return count;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public string LastName
        {
            get
            {
                return this.lastName;
            }
            set
            {
                this.lastName = value;
            }
        }

        public string UserName
        {
            get
            {
                return this.username;
            }
            set
            {
                this.username = value;
            }
        }

        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
            }
        }
        public string Access
        {
            get
            {
                return this.access;
            }
            set
            {
                this.access = value;
            }
        }

        public int Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        public bool Active
        {
            get
            {
                return this.active;
            }
            set
            {
                this.active = value;
            }
        }
    }
}
