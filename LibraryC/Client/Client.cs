﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryC.Client
{
    public class Client
    {
        /// <summary>
        /// State Variables
        /// </summary>
        private int id;
        private string name;
        private string lastName;
        private string adress;
        private string citizenCard;
        private bool active;
        private static int count;
        private int idUser;

        /// <summary>
        /// Constructer
        /// </summary>
        /// <param name="id">Id of the client</param>
        /// <param name="name">Name of the client</param>
        /// <param name="lastName">Last name of the client</param>
        /// <param name="adress">Adress of the client</param>
        /// <param name="citizenCard">Number of the citizen card of the client</param>
        /// <param name="active">If the user is active or not</param>
        /// <param name="idUser">Id of the user that can use the account</param>
        public Client(int id, string name, string lastName, string adress, string citizenCard, bool active, int idUser)
        {
            this.id = id;
            this.name = name;
            this.lastName = lastName;
            this.adress = adress;
            this.citizenCard = citizenCard;
            this.active = active;
            this.idUser = idUser;
            count++;
        }

        /// <summary>
        /// Proprieties
        /// </summary>
        public static int Count
        {
            get
            {
                return count;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public string LastName
        {
            get
            {
                return this.lastName;
            }
            set
            {
                this.lastName = value;
            }
        }

        public string Adress
        {
            get
            {
                return this.adress;
            }
            set
            {
                this.adress = value;
            }
        }

        public string CitizenCard
        {
            get
            {
                return this.citizenCard;
            }
            set
            {
                this.citizenCard = value;
            }
        }

        public int Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        public bool Active
        {
            get
            {
                return this.active;
            }
            set
            {
                this.active = value;
            }
        }

        public int IdUser
        {
            get
            {
                return this.idUser;
            }
            set
            {
                this.idUser = value;
            }
        }

    }
}
