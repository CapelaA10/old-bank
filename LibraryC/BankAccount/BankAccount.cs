﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryC.BankAccount
{
    public class BankAccount
    {
        /// <summary>
        /// State Variables
        /// </summary>
        private int idAccount;
        private int idClient;
        private bool active;
        private static int count;
        private float finalAmount;

        /// <summary>
        /// Constructer for the bank account 
        /// </summary>
        /// <param name="idAccount">Id for the account</param>
        /// <param name="idClient">Id of the client for the account</param>
        /// <param name="active">If the account is active or not </param>
        public BankAccount(int idAccount, int idClient, bool active, float finalAmount)
        {
            this.idAccount = idAccount;
            this.idClient = idClient;
            this.active = active;
            this.finalAmount = finalAmount;
            count++;
        }

        /// <summary>
        /// Proprieties
        /// </summary>
        public int IdAccount
        {
            get
            {
                return this.idAccount;
            }
            set
            {
                this.idAccount = value;
            }
        }

        public int IdClient
        {
            get
            {
                return this.idClient;
            }
            set
            {
                this.idClient = value;
            }
        }

        public bool Active
        {
            get
            {
                return this.active;
            }
            set
            {
                this.active = value;
            }
        }

        public static int Count
        {
            get
            {
                return count;
            }
        }

        public float FinalAmount
        {
            get
            {
                return this.finalAmount;
            }
            set
            {
                this.finalAmount = value;
            }
        }

        public string StringFinalAmount
        {
            get
            {
                return this.finalAmount.ToString();
            }
        }
    }
}
