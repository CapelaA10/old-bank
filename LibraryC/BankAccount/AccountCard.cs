﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryC.BankAccount
{
    public class AccountCard
    {
        /// <summary>
        /// State Variables
        /// </summary>
        private int idCard;
        private int idAccount;
        private string cardNumber;
        private bool active;
        private static int count;

        /// <summary>
        /// Constructer
        /// </summary>
        /// <param name="idClient">Id of the client</param>
        /// <param name="idAccount">Id of the Account</param>
        /// <param name="cardNumber">Card number</param>
        /// <param name="active">If the card is active or not</param>
        public AccountCard(int idCard,int idAccount, string cardNumber, bool active)
        {
            this.idCard = idCard;
            this.idAccount = idAccount;
            this.cardNumber = cardNumber;
            this.active = active;
            count++;
        }

        /// <summary>
        /// Proprieties
        /// </summary>
        public int IdCard
        {
            get
            {
                return this.idCard;
            }
            set
            {
                this.idCard = value;
            }
        }

        public int IdAccount
        {
            get
            {
                return this.idAccount;
            }
            set
            {
                this.idAccount = value;
            }
        }

        public string CardNumber
        {
            get
            {
                return this.cardNumber;
            }
            set
            {
                this.cardNumber = value;
            }
        }

        public bool Active
        {
            get
            {
                return this.active;
            }
            set
            {
                this.active = value;
            }
        }

        public static int Count
        {
            get
            {
                return count;
            }
        }
    }
}
