﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryC.Movement
{
    public class Movement
    {
        /// <summary>
        /// Proprieties
        /// </summary>
        public int Id { get; set; }
        public DateTime MovementDate { get; set; }
        public float Amount { get; set; }
        public int IdBa { get; set; }
        public string TypeOfMovement { get; set; }

        /// <summary>
        /// Proprieties
        /// </summary>
        /// <param name="id">Id of the movement</param>
        /// <param name="movementDate">Movement date</param>
        /// <param name="amount">Amount of the movement</param>
        /// <param name="idBa">Id of the bank account</param>
        /// <param name="typeOfMovement">This type of movement is to now if it is to add money or cash out the money</param>
        public Movement(int id, DateTime movementDate, float amount, int idBa, string typeOfMovement)
        {
            this.Id = id;
            this.MovementDate = movementDate;
            this.Amount = amount;
            this.IdBa = idBa;
            this.TypeOfMovement = typeOfMovement;
        }
    }
}
