﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Class.User.Subc
{
    public class Employ : User
    {
        /// <summary>
        /// State Variables
        /// </summary>
        private string name;
        private string lastName;
        private string username;
        private string password;
        private string tokenAcess;
        private int id;
        private bool active;

        /// <summary>
        /// Constructer
        /// </summary>
        /// <param name="name">Name of the employ</param>
        /// <param name="lastName">Last Name of the employ</param>
        /// <param name="username">User name for login</param>
        /// <param name="password">Password for login </param>
        /// <param name="tokenAcess">Token to check if it is a manager or an employ</param>
        /// <param name="id">Id of the employ</param>
        /// <param name="active">If the employ is active or not</param>
        public Employ(string name, string lastName, string username, string password, string tokenAcess, int id, bool active) : base(name, lastName, username, password, tokenAcess, id, active)
        {
            this.name = name;
            this.lastName = lastName;
            this.username = username;
            this.password = password;
            this.tokenAcess = tokenAcess;
            this.id = id;
            this.active = active;
        }
    }
}
