﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;
using System.Configuration;

namespace AppTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //Provider from app.config
            string provider = "System.Data.SqlClient";

            //Connection string from app.config
            string connectionString = @"Data Source = CAPELA\SQLEXPRESS; Initial Catalog = OLDBANK; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = False; ApplicationIntent = ReadWrite; MultiSubnetFailover = False";

            //Db provider creator
            DbProviderFactory factory = DbProviderFactories.GetFactory(provider);

            //Using and test connection
            using (DbConnection connection = factory.CreateConnection())
            {
                if (connection == null)
                {
                    Console.WriteLine("Connection Error");
                    Console.ReadKey();
                    return;
                }

                //Equal value
                connection.ConnectionString = connectionString;

                //Openning connection
                connection.Open();

                //Creating a new command
                DbCommand command = factory.CreateCommand();

                //Check if the command 
                if(command == null)
                {
                    Console.WriteLine("Command Error");
                    Console.ReadKey();
                    return;
                }

                //Equal value of connection
                command.Connection = connection;

                //SQL commando to select the table
                command.CommandText = "SELECT * FROM USERS";

                //Using and writing in the console data of the DB 
                using (DbDataReader dataReader = command.ExecuteReader())
                {
                    while(dataReader.Read())
                    {
                        Console.WriteLine($"{dataReader["U_NAME"]}");
                    }
                }

                command.CommandText = "SELECT * FROM CLIENTS";

                using (DbDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        Console.WriteLine($"{dataReader["C_LASTNAME"]}");
                    }
                }

                //Waiting for the user in the console to write someting to continue
                Console.ReadLine();
            }
        }
    }
}
