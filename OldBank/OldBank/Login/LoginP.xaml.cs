﻿using LibraryC.User;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Configuration;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using OldBank.MainMenu;

namespace OldBank.Login
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginP : ContentPage
    {
        public LoginP()
        {
            InitializeComponent();
            LoadUsers();
        }

        //Creating a list for transfer the data from the DB to the list and to use the check login
        List<User> users = new List<User>(); 

        public void LoadUsers()
        {
            //Connection string to connect to the db 
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=OldBank;User ID=capela_;Password=28021989Pi;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {

                    //Command string
                    string command = "SELECT U_NAME, U_LASTNAME, U_USERNAME, U_PASSWORD,U_ACESS ,ID_USER,  U_ACTIVE FROM USERS";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Using datareader
                    using (SqlDataReader reader = cmm.ExecuteReader())
                    {
                        //Check and while was info in
                        while (reader.Read())
                        {
                            //Creating the users with the new info about
                            users.Add(new User(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetInt32(5), reader.GetBoolean(6)));
                        }
                    }

                    //Closing connection
                    cnn.Close();
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }
        }

        private void CheckLogin_Clicked(object sender, EventArgs e)
        {
            //Variables
            string username;
            string password;

            //Incializing variables
            username = UsernameHolder.Text;
            password = PasswordHolder.Text;

            //Cicle for to see all users
            for (int i = 0; i < users.Count; i++)
            {
                //If to check if the user have is password right and is username
                if (username == users[i].UserName && password == users[i].Password)
                {
                    //Display an alert to the user see it loged in to the app
                    DisplayAlert("Login", "You Are In", "Thank You");

                    //Check if the user is a manager or a employ 
                    if (users[i].Access == "M")
                    {
                        Navigation.PushModalAsync(new MenuM());
                    }
                    else if (users[i].Access == "E")
                    {
                        Navigation.PushModalAsync(new MenuE());
                    }
                    else
                    {
                        //Error with the premission that the manager did
                        DisplayAlert("ERROR", "SOMETHING WENT WRONG WITH YOUR PREMISSION, PLEASE CONTACT A MANAGER", "EXIT");
                    }
                }
            }
        }
    }
}