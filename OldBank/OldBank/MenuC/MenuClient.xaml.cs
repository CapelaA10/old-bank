﻿using OldBank.MenuC.SubMenu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.MenuC
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuClient : ContentPage
	{
		public MenuClient()
		{
			InitializeComponent ();
		}

        private void ShowClient_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ShowClient());
        }

        private void InsertClient_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new InsertClient());
        }

        private void DeleteClient_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new DeleteClient());
        }

        private void UpdateClient_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new UpdateClient());
        }
    }
}