﻿using LibraryC.Client;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.MenuC.SubMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShowClient : ContentPage
	{
		public ShowClient ()
		{
			InitializeComponent ();
            LoadClients();
		}
        
        //List of clients
        List<Client> clients = new List<Client>();

        public void LoadClients()
        {
            //Connection string to connect to the db 
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=OldBank;User ID=capela_;Password=28021989Pi;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {

                    //Command string
                    string command = "SELECT ID_CLIENT, C_NAME, C_LASTNAME, C_ADRESS, C_CITIZENCARD, C_ACTIVE, C_ID_USER FROM CLIENTS";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Using datareader
                    using (SqlDataReader reader = cmm.ExecuteReader())
                    {
                        //Check and while was info in
                        while (reader.Read())
                        {
                            if (reader.GetBoolean(5) == true)
                            {
                                //Creating the users with the new info about
                                clients.Add(new Client(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetBoolean(5), reader.GetInt32(6)));
                            }
                            
                        }
                    }

                    //Closing connection
                    cnn.Close();
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }

            //Passing the clients list to the listview
            ListViewClient.ItemsSource = clients;
        }
    }
}