﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.MenuC.SubMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InsertClient : ContentPage
	{
		public InsertClient ()
		{
			InitializeComponent ();
		}

        private void InsertClientB_Clicked(object sender, EventArgs e)
        {
            //Connection string to connect to the db 
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=OldBank;User ID=capela_;Password=28021989Pi;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //Command string
                    string command = "INSERT INTO CLIENTS(ID_CLIENT, C_NAME, C_LASTNAME, C_ADRESS, C_CITIZENCARD, C_ACTIVE, C_ID_USER) VALUES (@ID, @NAME, @LASTNAME, @ADRESS, @CITIZENCARD, @ACTIVE, @ID_USER);";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Add parameters so the query know what is getting add
                    cmm.Parameters.AddWithValue("@ID", IdClient.Text);
                    cmm.Parameters.AddWithValue("@NAME", NameClient.Text);
                    cmm.Parameters.AddWithValue("@LASTNAME", LastName.Text);
                    cmm.Parameters.AddWithValue("@ADRESS", ClientAdress.Text);
                    cmm.Parameters.AddWithValue("@CITIZENCARD", ClientCitizenCard.Text);
                    cmm.Parameters.AddWithValue("@ACTIVE", ClientActive.Text);
                    cmm.Parameters.AddWithValue("@ID_USER", ManagerUser.Text);

                    //Execute
                    cmm.ExecuteNonQuery();

                    //Closing connection
                    cnn.Close();

                    //Display info and getting back to the client menu
                    DisplayAlert("Client", "is inserted", "Thank you");
                    Navigation.PushModalAsync(new MenuClient());
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }
        }
    }
}