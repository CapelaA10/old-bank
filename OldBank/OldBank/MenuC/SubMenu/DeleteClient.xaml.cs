﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.MenuC.SubMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DeleteClient : ContentPage
	{
		public DeleteClient ()
		{
			InitializeComponent ();
		}

        private void DeleteUserB_Clicked(object sender, EventArgs e)
        {
            //Try catch to check if it is a error in the db connection
            try
            {
                //Connection string to connect to the db 
                string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=OldBank;User ID=capela_;Password=28021989Pi;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //Command string
                    string command = "UPDATE CLIENTS SET C_ACTIVE=0 WHERE ID_CLIENT=@ID";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Add parameters so the query know what is getting add
                    cmm.Parameters.AddWithValue("@ID", IdClient.Text);

                    //Execute
                    cmm.ExecuteNonQuery();

                    //Closing connection
                    cnn.Close();

                    //Display info and getting back to the Client menu
                    DisplayAlert("Client", "is deleted", "Thank you");
                    Navigation.PushModalAsync(new MenuClient());
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }
        }
    }
}