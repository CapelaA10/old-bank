﻿using LibraryC.Client;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.MenuC.SubMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UpdateClient : ContentPage
	{
		public UpdateClient ()
		{
            InitializeComponent();
		}

        private void ClientUpdate_Clicked(object sender, EventArgs e)
        {
            //Connection string to connect to the db 
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=OldBank;User ID=capela_;Password=28021989Pi;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //Command string
                    string command = "UPDATE CLIENTS SET ID_CLIENT=@ID, C_NAME=@NAME, C_LASTNAME=@LASTNAME, C_ADRESS=@ADRESS, C_CITIZENCARD=@CITIZENCARD, C_ACTIVE=@ACTIVE, C_ID_USER=@USERIDMANAGER WHERE ID_CLIENT=@ID";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Add parameters so the query know what is getting add
                    cmm.Parameters.AddWithValue("@ID", IdClient.Text);
                    cmm.Parameters.AddWithValue("@NAME", Name.Text);
                    cmm.Parameters.AddWithValue("@LASTNAME", LastName.Text);
                    cmm.Parameters.AddWithValue("@ADRESS", Adress.Text);
                    cmm.Parameters.AddWithValue("@CITIZENCARD", CitizenCard.Text);
                    cmm.Parameters.AddWithValue("@ACTIVE", Active.Text);
                    cmm.Parameters.AddWithValue("@USERIDMANAGER", UserIdManeger.Text);

                    //Execute
                    cmm.ExecuteNonQuery();

                    //Closing connection
                    cnn.Close();

                    //Display info and getting back to the client menu
                    DisplayAlert("Client", "is updated", "Thank you");
                    Navigation.PushModalAsync(new MenuClient());
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }
        }
    }
}