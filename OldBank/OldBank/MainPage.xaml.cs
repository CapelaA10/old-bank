﻿using OldBank.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace OldBank
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        //Enter the login menu
        private void Login_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new LoginP());
        }
    }
}
