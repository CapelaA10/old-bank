﻿using OldBank.BAccountM;
using OldBank.MenuC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.MainMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuE : ContentPage
	{
		public MenuE ()
		{
			InitializeComponent ();
		}

        //Button to acess the menu of the clients so the user can add, edit, delete or update an client
        private void AcessClientsMenu_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new MenuClient());
        }

        //Button to acess the menu of the bank accounts in that menu the user can add edit and delete the bank account and the user will have 2 more menu choices about the movements in the bank account and the cards in the bank account
        private void AcessBankAccounts_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new AccountManag());
        }
    }
}