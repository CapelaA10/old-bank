﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using OldBank.EmployM;
using OldBank.MenuC;
using OldBank.BAccountM;

namespace OldBank.MainMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuM : ContentPage
	{
		public MenuM ()
		{
			InitializeComponent ();
		}

        //Button to acess the menu of the employs so you can add edit or delete an employ and see the employs
        private void AcessEmployMenu_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new EmployMenu());
        }


        //Button to acess the menu of the clients so the user can add, edit, delete or update an client
        private void AcessClientsMenu_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new MenuClient());
        }

        //Button to acess the menu of the bank accounts in that menu the user can add edit and delete the bank account and the user will have 2 more menu choices about the movements in the bank account and the cards in the bank account
        private void AcessBankAccounts_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new AccountManag());
        }
    }
}