﻿using OldBank.BAccountM.CardAccountM;
using OldBank.BAccountM.MovementM;
using OldBank.BAccountM.SubMenuBA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.BAccountM
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AccountManag : ContentPage
	{
		public AccountManag ()
		{
			InitializeComponent ();
		}

        private void ShowBankAccounts_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ShowBankAccount());
        }

        private void InsertBankAccount_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new InsertBankAccount());
        }

        private void DeleteBankAccount_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new DeleteBankAccount());
        }

        private void UpdateBankAccount_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new UpdateBankAccount());
        }

        private void CardAccountMenu_Clicked(object sender, EventArgs e)
        {
             Navigation.PushModalAsync(new CardMenu());
        }

        private void MovementsMenu_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new MovmentsMenu());
        }
    }
}