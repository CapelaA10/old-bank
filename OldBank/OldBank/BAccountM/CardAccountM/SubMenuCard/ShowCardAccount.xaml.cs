﻿using LibraryC.BankAccount;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.BAccountM.CardAccountM.SubMenuCard
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShowCardAccount : ContentPage
	{
		public ShowCardAccount ()
		{
			InitializeComponent ();
            LoadCardAccount();
		}

        //List to the card account
        List<AccountCard> accountCards = new List<AccountCard>();

        private void LoadCardAccount()
        {
            //Connection string to connect to the db 
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=OldBank;User ID=capela_;Password=28021989Pi;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {

                    //Command string
                    string command = "SELECT  ID_CARD, AC_CARDNUMBER, AC_ACTIVE, ID_BAS FROM ACCOUNT_CARDS";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Using datareader
                    using (SqlDataReader reader = cmm.ExecuteReader())
                    {
                        //Check and while was info in
                        while (reader.Read())
                        {
                            //Creating the card accounts with the new info about
                            accountCards.Add(new AccountCard(reader.GetInt32(0), reader.GetInt32(3), reader.GetString(1), reader.GetBoolean(2)));

                        }
                    }

                    //Closing connection
                    cnn.Close();
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }

            ListViewCardAccount.ItemsSource = accountCards;
        }
	}
}