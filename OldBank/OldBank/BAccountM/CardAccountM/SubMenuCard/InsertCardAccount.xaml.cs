﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.BAccountM.CardAccountM.SubMenuCard
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InsertCardAccount : ContentPage
	{
		public InsertCardAccount ()
		{
			InitializeComponent ();
		}

        private void InsertAccountCardBtt_Clicked(object sender, EventArgs e)
        {
            //Connection string to connect to the db 
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=OldBank;User ID=capela_;Password=28021989Pi;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {

                    //Command string
                    string command = "INSERT INTO ACCOUNT_CARDS(ID_CARD, AC_CARDNUMBER, AC_ACTIVE, ID_BAS) VALUES ((SELECT MAX(ID_CARD)+1 FROM ACCOUNT_CARDS), @CARDNUMBER, 1, @IDBANKACCOUNT);";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Add parameters so the query know what is getting add
                    cmm.Parameters.AddWithValue("@CARDNUMBER", CardNumberEntry.Text);
                    cmm.Parameters.AddWithValue("@IDBANKACCOUNT", IdBankAccountEntry.Text);
                    
                    //Execute
                    cmm.ExecuteNonQuery();

                    //Closing connection
                    cnn.Close();

                    //Display info and getting back to the card menu
                    DisplayAlert("Card Account", "is inserted", "Thank you");
                    Navigation.PushModalAsync(new CardMenu());
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }
        }
    }
}