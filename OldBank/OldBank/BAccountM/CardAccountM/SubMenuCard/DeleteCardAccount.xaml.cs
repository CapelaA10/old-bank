﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.BAccountM.CardAccountM.SubMenuCard
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DeleteCardAccount : ContentPage
	{
		public DeleteCardAccount ()
		{
			InitializeComponent ();
		}

        private void DeleteCardAccountBtt_Clicked(object sender, EventArgs e)
        {
            //Try catch to check if it is a error in the db connection
            try
            {
                //Connection string to connect to the db 
                string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=OldBank;User ID=capela_;Password=28021989Pi;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {

                    //Command string
                    string command = "UPDATE ACCOUNT_CARDS SET AC_ACTIVE=0 WHERE ID_CARD=@ID";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Add parameters so the query know what is getting add
                    cmm.Parameters.AddWithValue("@ID", IdAccountCardEntry.Text);

                    //Execute
                    cmm.ExecuteNonQuery();

                    //Closing connection
                    cnn.Close();

                    //Display info and getting back to the Card Menu menu
                    DisplayAlert("Account Card", "is deleted", "Thank you");
                    Navigation.PushModalAsync(new CardMenu());
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }
        }
    }
}