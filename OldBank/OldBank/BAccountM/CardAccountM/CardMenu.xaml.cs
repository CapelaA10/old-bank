﻿using OldBank.BAccountM.CardAccountM.SubMenuCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.BAccountM.CardAccountM
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CardMenu : ContentPage
	{
		public CardMenu ()
		{
			InitializeComponent ();
		}

        private void ShowCard_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ShowCardAccount());
        }

        private void InsertCard_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new InsertCardAccount());
        }

        private void DeleteCard_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new DeleteCardAccount());
        }
    }
}