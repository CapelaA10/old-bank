﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.BAccountM.SubMenuBA
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateBankAccount : ContentPage
    {
        public UpdateBankAccount()
        {
            InitializeComponent();
        }

        private void UpdateBankAccountBtt_Clicked(object sender, EventArgs e)
        {
            //Connection string to connect to the db 
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=OldBank;User ID=capela_;Password=28021989Pi;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch to see if it was a error in the database connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //Command string
                    string command = "UPDATE BANK_ACCOUNTS SET ID_BA=@ID, BA_ACTIVE=1, BALANCE=0, CLIENT_ID=@IDCLIENT WHERE ID_BA=@ID";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Add parameters so the query know what is getting add
                    cmm.Parameters.AddWithValue("@ID", BankAccountId.Text);
                    cmm.Parameters.AddWithValue("@IDCLIENT", ClientIdPlaceHolder.Text);

                    //Execute
                    cmm.ExecuteNonQuery();

                    //Closing connection
                    cnn.Close();

                    //Display info and getting back to the bank account menu
                    DisplayAlert("Bank Account", "is updated", "Thank you");
                    Navigation.PushModalAsync(new AccountManag());
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }
        }
    }
}