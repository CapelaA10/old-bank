﻿using LibraryC.BankAccount;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.BAccountM.SubMenuBA
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShowBankAccount : ContentPage
    {
        public ShowBankAccount()
        {
            InitializeComponent();
        }

        //New list of the bank accounts
        List<BankAccount> bankAccounts = new List<BankAccount>();

        private void SearchClientBankAccounts_Clicked(object sender, EventArgs e)
        {
            //Connection string to connect to the db 
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=OldBank;User ID=capela_;Password=28021989Pi;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Variables
            float finalBalance;
            string idClient;

            //Inicializating a variable
            idClient = IdClient.Text;

            //Geting the final balance
            finalBalance = GetBalance(idClient);

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //Command string
                    string command = "SELECT ID_BA, CLIENT_ID, BA_ACTIVE FROM BANK_ACCOUNTS WHERE CLIENT_ID=@ID";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();
                    
                    //Add parameters so the query know what is getting add
                    cmm.Parameters.AddWithValue("@ID", IdClient.Text);

                    //Using datareader
                    using (SqlDataReader reader = cmm.ExecuteReader())
                    {
                        //Check and while was info in
                        while (reader.Read())
                        {
                            bankAccounts.Add(new BankAccount(reader.GetInt32(0), reader.GetInt32(1), reader.GetBoolean(2), finalBalance));
                        }
                    }

                    //Closing connection
                    cnn.Close();
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }

            //List to listview to show in the screen
            ListViewBankAccounts.ItemsSource = bankAccounts;
        }

        public float GetBalance(string idClient)
        {
            //Connection string
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=OldBank;User ID=capela_;Password=28021989Pi;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Local Variables
            float finalBalance = 0;

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //Command string
                    string command = "SELECT AMOUNT, TypeOfMovement FROM MOVEMENT INNER JOIN BANK_ACCOUNTS ON BANK_ACCOUNTS.ID_BA = MOVEMENT.ID_BA WHERE BANK_ACCOUNTS.CLIENT_ID = @ID";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Add parameters so the query know what is getting add
                    cmm.Parameters.AddWithValue("@ID", IdClient.Text);

                    //Using datareader
                    using (SqlDataReader reader = cmm.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            //If it is cash in or cash out
                            if (reader.GetString(1) == "CI")
                            {
                                finalBalance = finalBalance + reader.GetFloat(0);
                            }
                            else if(reader.GetString(1) == "CO")
                            {
                                finalBalance = finalBalance - reader.GetFloat(0);
                            }
                        }
                    }

                    //Closing connection
                    cnn.Close();
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }

            //Returning the value
            return finalBalance;
        }
    }
}