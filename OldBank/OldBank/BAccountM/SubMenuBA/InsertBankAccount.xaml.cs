﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.BAccountM.SubMenuBA
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InsertBankAccount : ContentPage
    {
        public InsertBankAccount()
        {
            InitializeComponent();
        }

        private void InsertBankAccountBtt_Clicked(object sender, EventArgs e)
        {
            //Connection string to connect to the db 
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=OldBank;User ID=capela_;Password=28021989Pi;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {

                    //Command string
                    string command = "INSERT INTO BANK_ACCOUNTS (ID_BA, BA_ACTIVE, BALANCE, CLIENT_ID) VALUES ((SELECT MAX(ID_BA)+1 FROM BANK_ACCOUNTS), 1, 0, @CLIENT_ID);";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Add parameters so the query know what is getting add
                    cmm.Parameters.AddWithValue("@CLIENT_ID", IdClient.Text);

                    //Execute
                    cmm.ExecuteNonQuery();

                    //Closing connection
                    cnn.Close();

                    //Display info and getting back to the account menu menu
                    DisplayAlert("Bank Account", "is inserted", "Thank you");
                    Navigation.PushModalAsync(new AccountManag());
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }
        }
    }
}