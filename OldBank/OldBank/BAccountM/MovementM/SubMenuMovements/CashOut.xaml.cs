﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.BAccountM.MovementM.SubMenuMovements
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CashOut : ContentPage
	{
		public CashOut ()
		{
			InitializeComponent ();
		}

        private void CashOutBtt_Clicked(object sender, EventArgs e)
        {
            //Connection string to connect to the db 
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=OldBank;User ID=capela_;Password=28021989Pi;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //Variables date time today 
                    DateTime today = DateTime.Today;

                    //Command string
                    string command = "INSERT INTO MOVEMENT(ID_MOV, MOV_DATE, AMOUNT, ID_BA, TypeOfMovement) VALUES ((SELECT MAX(ID_MOV)+1 FROM MOVEMENT), @MOVDATE, @AMOUNT, @IDBA, @TYPEOFMOVEMENT)";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Add parameters with value to display the movements from the id of the bankaccount
                    cmm.Parameters.AddWithValue("@MOVDATE", today.ToString());
                    cmm.Parameters.AddWithValue("@AMOUNT", AmountEntry.Text);
                    cmm.Parameters.AddWithValue("@IDBA", BankAccountIdEntry.Text);
                    cmm.Parameters.AddWithValue("@TYPEOFMOVEMENT", "CO");

                    //Execute
                    cmm.ExecuteNonQuery();

                    //Closing connection
                    cnn.Close();

                    //Display alert to the user
                    DisplayAlert("Movement", "Is done", "OK");
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }
        }
    }
}