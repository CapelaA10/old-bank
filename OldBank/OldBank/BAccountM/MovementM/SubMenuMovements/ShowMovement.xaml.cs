﻿using LibraryC.Movement;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.BAccountM.MovementM.SubMenuMovements
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShowMovement : ContentPage
	{
		public ShowMovement ()
		{
			InitializeComponent ();
		}

        //Creating a list of movements
        List<Movement> movements = new List<Movement>();

        private void ShowMovementsBtt_Clicked(object sender, EventArgs e)
        {
            //Connection string to connect to the db 
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=OldBank;User ID=capela_;Password=28021989Pi;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {

                    //Command string
                    string command = "SELECT  ID_MOV , MOV_DATE, AMOUNT, ID_BA, TypeOfMovement FROM MOVEMENT WHERE ID_BA = @ID";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Add parameters with value to display the movements from the id of the bankaccount
                    cmm.Parameters.AddWithValue("@ID", IdBankAccountEntry.Text);

                    //Opening connection
                    cnn.Open();

                    //Using datareader
                    using (SqlDataReader reader = cmm.ExecuteReader())
                    {
                        //Check and while was info in
                        while (reader.Read())
                        {
                            //Add to the list the movements
                            movements.Add(new Movement(reader.GetInt32(0), reader.GetDateTime(1), reader.GetFloat(2), reader.GetInt32(3), reader.GetString(4)));
                        }
                    }

                    //Closing connection
                    cnn.Close();
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }

            //Puting values into the itemsource of the list view in this case the movements list 
            ListViewMovements.ItemsSource = movements;
        }
    }
}
