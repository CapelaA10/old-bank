﻿using OldBank.BAccountM.MovementM.SubMenuMovements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.BAccountM.MovementM
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MovmentsMenu : ContentPage
	{
		public MovmentsMenu ()
		{
			InitializeComponent ();
		}

        private void Deposit_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new DepositMoneyPage());
        }

        private void ShowMovements_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ShowMovement());
        }

        private void CashOut_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new CashOut());
        }
    }
}