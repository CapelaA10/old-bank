﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.EmployM.SubMenuM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateEmploy : ContentPage
    {
        public UpdateEmploy()
        {
            InitializeComponent();
        }

        private void UpdateEmployClicked(object sender, EventArgs e)
        {
            //Connection string to connect to the db 
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=OldBank;User ID=capela_;Password=28021989Pi;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //Command string
                    string command = "UPDATE USERS SET ID_USER=@ID, U_NAME=@NAME, U_LASTNAME=@LASTNAME, U_USERNAME=@USERNAME, U_PASSWORD=@PASSWORD, U_ACESS=@ACESS, U_ACTIVE=@ACTIVE WHERE ID_USER=@ID";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Add parameters so the query know what is getting add
                    cmm.Parameters.AddWithValue("@ID", IdUser .Text);
                    cmm.Parameters.AddWithValue("@NAME", Name.Text);
                    cmm.Parameters.AddWithValue("@LASTNAME", LastName.Text);
                    cmm.Parameters.AddWithValue("@USERNAME", UserName.Text);
                    cmm.Parameters.AddWithValue("@PASSWORD", PasswordUser.Text);
                    cmm.Parameters.AddWithValue("@ACESS", AcessUser.Text);
                    cmm.Parameters.AddWithValue("@ACTIVE", ActiveUser.Text);

                    //Execute
                    cmm.ExecuteNonQuery();

                    //Closing connection
                    cnn.Close();

                    //Display info and getting back to the employ menu
                    DisplayAlert("User", "The user is updated", "Thank you");
                    Navigation.PushModalAsync(new EmployMenu());
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }
        }
    }
}