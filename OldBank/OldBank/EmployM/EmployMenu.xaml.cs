﻿using OldBank.EmployM.SubMenuM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OldBank.EmployM
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EmployMenu : ContentPage
	{
		public EmployMenu()
		{
			InitializeComponent ();
		}

        private void ShowEmploys_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ShowEmploys());
        }

        private void InsertEmploy_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new InsertEmploy());
        }

        private void DeleteEmploy_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new DeleteEmploy());
        }

        private void UpdateEmployM_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new UpdateEmploy());
        }
    }
}